﻿var debugMode = false;
var rightHanded = true;

// On Load
$(function () {

  // TODO: perform initial setup: just a choice of the dominant hand


  showInstruction("No LeapMotion Detected.");



});

//Displays the popup setting at the page loading
function setupHand(mylink, windowname) {
    if (!window.focus) return true;
    var href;
    if (typeof (mylink) == 'string')
        href = mylink;
    else
        href = mylink.href;
    window.open(href, windowname, 'width=400,height=300,position=fixed,left=300');
    return false;
}

function showInstruction(instruction) {
  $('#lmInstruction').html(instruction);
}

// Sets debug info on and off
function toggleDebug() {
  debugMode = !debugMode;

  if (debugMode) {
    $('#btn_debug').html("Debug ON");
  } else {
    $('#btn_debug').html("Debug OFF");
  }
}

// Sets handedness left or right
function toggleHandedness() {
  rightHanded = !rightHanded;

  if (rightHanded) {
    $('#btn_handedness').html("Right Handed");
  } else {
    $('#btn_handedness').html("Left Handed");
  }
}

// Returns a string based on the handedness settings: "Left" or "Right"
function getHand() {
  return rightHanded ? "Right" : "Left";
}

// Returns a string based on the inverse handedness settings: "Left" or "Right"
function getNotHand() {
  return rightHanded ? "Left" : "Right";
}