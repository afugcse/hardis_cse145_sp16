﻿/*
// Store frame for motion functions
var previousFrame = null;
var paused = false;

// Setup Leap loop with frame callback function
var controllerOptions = {
  enableGestures: false
  //optimizeHMD: true // to use HMD mode
};

Leap.loop(controllerOptions, function (frame) {
  if (paused) {
    return; // Skip this update
  }

  // Display Frame object data
  var frameOutput = $('#frameData');

  var frameString = "Frame ID: " + frame.id + "<br />"
                  + "Timestamp: " + frame.timestamp + " &micro;s<br />"
                  + "Hands: " + frame.hands.length + "<br />"
                  + "Fingers: " + frame.fingers.length + "<br />"
                  + "Tools: " + frame.tools.length + "<br />"
                  + "Gestures: " + frame.gestures.length + "<br />";

  // Frame motion factors
  if (previousFrame && previousFrame.valid) {
    var translation = frame.translation(previousFrame);
    frameString += "Translation: " + vectorToString(translation) + " mm <br />";

    var rotationAxis = frame.rotationAxis(previousFrame);
    var rotationAngle = frame.rotationAngle(previousFrame);
    frameString += "Rotation axis: " + vectorToString(rotationAxis, 2) + "<br />";
    frameString += "Rotation angle: " + rotationAngle.toFixed(2) + " radians<br />";

    var scaleFactor = frame.scaleFactor(previousFrame);
    frameString += "Scale factor: " + scaleFactor.toFixed(2) + "<br />";
  }
  frameOutput.innerHTML = "<div style='width:300px; float:left; padding:5px'>" + frameString + "</div>";

  // Display Hand object data
  var handOutput = $("#handData");
  var handString = "";
  if (frame.hands.length > 0) {
    for (var i = 0; i < frame.hands.length; i++) {
      var hand = frame.hands[i];

      handString += "<div style='width:300px; float:left; padding:5px'>";
      handString += "Hand ID: " + hand.id + "<br />";
      handString += "Type: " + hand.type + " hand" + "<br />";
      handString += "Direction: " + vectorToString(hand.direction, 2) + "<br />";
      handString += "Palm position: " + vectorToString(hand.palmPosition) + " mm<br />";
      handString += "Grab strength: " + hand.grabStrength + "<br />";
      handString += "Pinch strength: " + hand.pinchStrength + "<br />";
      handString += "Confidence: " + hand.confidence + "<br />";
      handString += "Arm direction: " + vectorToString(hand.arm.direction()) + "<br />";
      handString += "Arm center: " + vectorToString(hand.arm.center()) + "<br />";
      handString += "Arm up vector: " + vectorToString(hand.arm.basis[1]) + "<br />";

      // Hand motion factors
      if (previousFrame && previousFrame.valid) {
        var translation = hand.translation(previousFrame);
        handString += "Translation: " + vectorToString(translation) + " mm<br />";

        var rotationAxis = hand.rotationAxis(previousFrame, 2);
        var rotationAngle = hand.rotationAngle(previousFrame);
        handString += "Rotation axis: " + vectorToString(rotationAxis) + "<br />";
        handString += "Rotation angle: " + rotationAngle.toFixed(2) + " radians<br />";

        var scaleFactor = hand.scaleFactor(previousFrame);
        handString += "Scale factor: " + scaleFactor.toFixed(2) + "<br />";
      }

      // IDs of pointables associated with this hand
      if (hand.pointables.length > 0) {
        var fingerIds = [];
        for (var j = 0; j < hand.pointables.length; j++) {
          var pointable = hand.pointables[j];
          fingerIds.push(pointable.id);
        }
        if (fingerIds.length > 0) {
          handString += "Fingers IDs: " + fingerIds.join(", ") + "<br />";
        }
      }

      handString += "</div>";
    }
  }
  else {
    handString += "No hands";
  }
  handOutput.innerHTML = handString;

  // Display Pointable (finger and tool) object data
  var pointableOutput = $("#pointableData");
  var pointableString = "";
  if (frame.pointables.length > 0) {
    var fingerTypeMap = ["Thumb", "Index finger", "Middle finger", "Ring finger", "Pinky finger"];
    var boneTypeMap = ["Metacarpal", "Proximal phalanx", "Intermediate phalanx", "Distal phalanx"];
    for (var i = 0; i < frame.pointables.length; i++) {
      var pointable = frame.pointables[i];

      pointableString += "<div style='width:250px; float:left; padding:5px'>";

      if (pointable.tool) {
        pointableString += "Pointable ID: " + pointable.id + "<br />";
        pointableString += "Classified as a tool <br />";
        pointableString += "Length: " + pointable.length.toFixed(1) + " mm<br />";
        pointableString += "Width: " + pointable.width.toFixed(1) + " mm<br />";
        pointableString += "Direction: " + vectorToString(pointable.direction, 2) + "<br />";
        pointableString += "Tip position: " + vectorToString(pointable.tipPosition) + " mm<br />"
        pointableString += "</div>";
      }
      else {
        pointableString += "Pointable ID: " + pointable.id + "<br />";
        pointableString += "Type: " + fingerTypeMap[pointable.type] + "<br />";
        pointableString += "Belongs to hand with ID: " + pointable.handId + "<br />";
        pointableString += "Classified as a finger<br />";
        pointableString += "Length: " + pointable.length.toFixed(1) + " mm<br />";
        pointableString += "Width: " + pointable.width.toFixed(1) + " mm<br />";
        pointableString += "Direction: " + vectorToString(pointable.direction, 2) + "<br />";
        pointableString += "Extended?: " + pointable.extended + "<br />";
        pointable.bones.forEach(function (bone) {
          pointableString += boneTypeMap[bone.type] + " bone <br />";
          pointableString += "Center: " + vectorToString(bone.center()) + "<br />";
          pointableString += "Direction: " + vectorToString(bone.direction()) + "<br />";
          pointableString += "Up vector: " + vectorToString(bone.basis[1]) + "<br />";
        });
        pointableString += "Tip position: " + vectorToString(pointable.tipPosition) + " mm<br />";
        pointableString += "</div>";
      }
    }
  }
  else {
    pointableString += "<div>No pointables</div>";
  }
  pointableOutput.innerHTML = pointableString;

  // Store frame for motion functions
  previousFrame = frame;
})





function vectorToString(vector, digits) {
  if (typeof digits === "undefined") {
    digits = 1;
  }
  return "(" + vector[0].toFixed(digits) + ", "
             + vector[1].toFixed(digits) + ", "
             + vector[2].toFixed(digits) + ")";
}

function togglePause() {
  paused = !paused;

  if (paused) {
    $("#pause").html("Resume");
  } else {
    $("#pause").html("Pause");
  }
}
*/



// TODO: fix sensitivity equation, just multiplying by motionSensitivity is not enough
var motionSensitivity = 0.1; // 0 (less sensitive) to 1 (more sensitive)

// LeapMotion options
var controllerOptions = {
  enableGestures: false
};

// LeapMotion loop
Leap.loop(controllerOptions, function (frame) {
  // Get the number of hands currently in the frame
  var handsCount = frame.hands.length;

  switch (handsCount) {
    case 1:
      oneHandInFrame(frame);
      break;
    case 2:
      //twoHandsInFrame(frame);
      break;
    default:
      showInstruction("Have the palm of your " + getHand() + " hand face down.</br>" +
                      "Then, close your " + getHand() + " hand as a fist.</br>" +
                      "Now, enter your " + getHand() + " hand in the Leap Motion capture area.");
      return;
  }
})

function oneHandInFrame(frame) {
  var hand = frame.hands[0];

  // check if the user entered the correct hand in the frame
  if (hand.type.toLowerCase() != getHand().toLowerCase()) {
    showInstruction("I told you to enter your " + getHand() + " hand.");
    return;
  }

  // check if the palm of the hand is facing down
  var normal = hand.palmNormal;
  var normX = normal[0];
  var normZ = normal[2];
  // check that normX and normZ are within a n% away from the zero (choose n = 10)
  if ((normX < -10 || normX > 10) && (normZ < -10 || normZ > 10)) {
    showInstruction("I told you to enter your " + getHand() + " hand with the palm facing down.");
    return;
  }

  // check if the hand is opened or closed
  if (hand.grabStrength == 0) { // hand is opened
    showInstruction("I told you to enter your " + getHand() + " hand closed as a fist.");
    return;
  }

  // Show instruction that the user is ready to move the ROV
  showInstruction("Start moving your " + getHand() + " hand to move the OpenROV.</br>" +
                  "You can enter your " + getNotHand() + " hand to issue other commands." +
                  "Slowly open your " + getHand() + " hand fist to increase the ROV thrust.");

  // Get roll, pitch, and yaw
  var roll = hand.roll() * motionSensitivity * 100;
  var pitch = hand.pitch() * motionSensitivity * 100;
  var yaw = hand.yaw() * motionSensitivity * 100;
  var thrust = (1 - hand.grabStrength) * motionSensitivity * 1000;

  var lmFeedback = $("#lmFeedback");
  var lmFeedbackStr = "";
  lmFeedbackStr += "<div style='float:left; padding:5px'>";
  lmFeedbackStr += "Roll: " + roll.toFixed(0) + "<br/>";
  lmFeedbackStr += "Pitch: " + pitch.toFixed(0) + "<br/>";
  lmFeedbackStr += "Yaw: " + yaw.toFixed(0) + "<br/>";
  lmFeedbackStr += "Thrust: " + thrust.toFixed(0) + "<br/>";
  lmFeedbackStr += "</div>";
  lmFeedback.html(lmFeedbackStr);
}
