// Constants
var normYrefValue = 1;

// Variables
var rightHanded = true; // constant for now, leater let user choose handedness from UI
var leapHandType;


(function (window, $, undefined) {
  'use strict';
  var leapmotion;
  var DISABLED = "DISABLED";
  leapmotion = function leapmotion(cockpit) {
    console.log('Loading leapmotion plugin in the browser.');
    var rov = this;
    // Instance variables
    rov.cockpit = cockpit;
    rov.power = 0.25;
    //default to mid power
    rov.vtrim = 0;
    //default to no trim
    rov.ttrim = 0;
    rov.tilt = 0;
    rov.light = 0;
    rov.sendToROVEnabled = true;
    rov.positions = {
      throttle: 0,
      yaw: 0,
      lift: 0,
      pitch: 0,
      roll: 0
    };
    rov.sendUpdateEnabled = true;
    var SAMPLE_PERIOD = 1000 / CONFIG.sample_freq;
    //ms
    var trimHeld = false;
    rov.priorControls = {};

    rov.bindingModel = {
      cockpit: rov.cockpit,
      thrustfactor: ko.observable(2),
      depthHoldEnabled: ko.observable(false),
      headingHoldEnabled: ko.observable(false),
      lasersEnabled: ko.observable(false),
      gamepadDisconnected: ko.observable(true),
      toggleDepthHold: rov.toggleholdDepth,
      toggleHeadingHold: rov.toggleholdHeading,
      toggleLasers: rov.toggleLasers
    };

    // Add required UI elements
    var jsFileLocation = urlOfJsFile('leapmotion.js');
    $('body').append('<div id="leapmotion-templates"></div>');
    $('#leapmotion-templates').load(jsFileLocation + '../ui-templates.html', function () {
      $('#footercontent').prepend('<div id="leapmotion_thrustfactor" data-bind="template: {name: \'template_rovPilot_thrustFactor\'}"></div>');
      ko.applyBindings(rov.bindingModel, document.getElementById('leapmotion_thrustfactor'));

      //$('#navtoolbar').append('<div id="leapmotion_navtoolbar" class="nav" data-bind="template: {name: \'template_leapmotion_navToolbar\'}"></div>');
      //ko.applyBindings(rov.bindingModel, document.getElementById('leapmotion_navtoolbar'));
    });

    /*
      $('#keyboardInstructions')
        .append('<p>press <i>i</i> to toggle lights</p>')
        .append('<p>press <i>[</i> to enable ESCs</p>')
        .append('<p>press <i>]</i> to disable ESCs</p>')
        .append('<p>press <i>m</i> to toggle heading hold (BETA)</p>')
        .append('<p>press <i>n</i> to toggle depth hold (BETA)</p>');
    */



    // for plugin management:
    this.name = 'leapmotion';   // for the settings
    this.viewName = 'LeapMotion interface'; // for the UI
    this.canBeDisabled = true; // allow enable/disable
    this.enable = function () {
      //alert('LeapMotion enabled');
      console.log('LeapMotion plugin enabled');
      rov.sendToROVEnabled = true;
    };
    this.disable = function () {
      //alert('LeapMotion disabled');
      console.log('LeapMotion plugin disabled');

      // force the motors to stop if the plugin gets disabled while using the leapmotion
      rov.setThrottle(0);
      rov.sendPilotingData();

      rov.sendToROVEnabled = false;
    };

    // Send piloting data updates
    setInterval(function () {
      rov.sendPilotingData();
    }, SAMPLE_PERIOD);

    // LeapMotion controller options
    var controllerOptions = {
      enableGestures: false
      //loopWhileDisconnected = false
    };

    // LeapMotion main loop
    Leap.loop(controllerOptions, function (frame) {

      // Do nothing if the plugin is disabled
      if (!rov.sendToROVEnabled) return;

      if (rightHanded) leapHandType = "right";
      else leapHandType = "left";

      // Get the number of hands currently in the frame
      var handsCount = frame.hands.length;

      switch (handsCount) {
        case 1:
        case 2:
          // loop through the hands currently in the frame
          for (var h = 0; h < frame.hands.length; h++) {
            // get current hand
            var hand = frame.hands[h];

            // dominant hand controller
            if (hand.type = leapHandType) {

              // === BEGIN THROTTLE MANAGEMENT =======================================================================

              // get hand normal vector
              var normal = hand.palmNormal;
              // get normal vector y component
              var normY = normal[1];

              // if the palm normal y component is less than -1 (points downward) set throttle forward (1)
              if (normY <= -normYrefValue) {
                rov.setThrottle(1);
              } // if the palm normal y component is greater than 1 (points upward) set throttle backward (-1)
              else if (normY >= normYrefValue) {
                rov.setThrottle(-1);
              } // else set throttle to 0
              else
                rov.setThrottle(0);

              // === END THROTTLE MANAGEMENT =========================================================================

              // === BEGIN POWER LEVEL MANAGEMENT ====================================================================

              var gs = hand.grabStrength;
              //rov.powerLevelAlternative(1 - parseFloat(gs).toPrecision(1));
              if (gs <= 0) rov.powerLevel(3);
              if (gs > 0 && gs <= 0.20) rov.powerLevel(3);
              if (gs > 0.20 && gs <= 0.40) rov.powerLevel(2);
              if (gs > 0.40 && gs <= 0.60) rov.powerLevel(2);
              if (gs > 0.60 && gs <= 0.90) rov.powerLevel(1);//4
              if (gs > 0.90) rov.powerLevel(1);//5

              // === END POWER LEVEL MANAGEMENT ======================================================================

              // === BEGIN MOTION MANAGEMENT =========================================================================
              
              //rov.setYaw(hand.yaw());
              //rov.setPitchControl(hand.pitch());

              // === END MOTION MANAGEMENT ===========================================================================
              
              // === BEGIN YAW MANAGEMENT =========================================================================
              
              var yaw = hand.yaw();
              if(yaw>0.10){
                rov.setYaw(1);
              }
              else{
                if(yaw<-0.10){
                  rov.setYaw(-1);
                }
                else{
                  rov.setYaw(0);
                }
              }

              // === END YAW MANAGEMENT ===========================================================================
              
              // === BEGIN LIFT MANAGEMENT =========================================================================
              
              var lift = hand.pitch();
              if(lift>0.20){
                rov.setLift(-1);
              }
              else{
                if(lift<-0.20){
                  rov.setLift(-1);
                }
                else{
                  rov.setLift(0);
                }
              }

              // === END LIFT MANAGEMENT ===========================================================================
              /* TODO: bind leapmotion behavior to rov commands below:
            
                setPitchControl
                setRollControl
              */
            }
            else { // non-dominand hand controller

              // === BEGIN LIGHTS MANAGEMENT ====================================================================

              var gs = hand.grabStrength;
              rov.adjustLights(gs);

              // === END LIGHTS MANAGEMENT ======================================================================

              // === BEGIN CAMERA MANAGEMENT ====================================================================

              rov.adjustCameraTilt(hand.pitch());

              // === END CAMERA MANAGEMENT ======================================================================

              /* TODO: bind leapmotion behavior to rov commands below:
                MAYBE: toggleLasers
              */
            }
          }
          break;
        default:
          // Reset everything to default
          rov.power = 0.25;
          rov.vtrim = 0;
          rov.ttrim = 0;
          rov.tilt = 0;
          rov.light = 0;
          rov.positions = {
            throttle: 0,
            yaw: 0,
            lift: 0,
            pitch: 0,
            roll: 0
          };

          return;
      }
    });
  };


  // === BEGIN ROV functions ===================================================================================

  var lastSentManualThrottle = {
    port: 0,
    vertical: 0,
    starbord: 0
  };

  leapmotion.prototype.disablePilot = function disablePilot() {
    this.sendToROVEnabled = false;
    console.log('disabled rov pilot.');
  };

  leapmotion.prototype.enablePilot = function enablePilot() {
    this.sendToROVEnabled = true;
    console.log('enabled rov pilot.');
  };

  leapmotion.prototype.manualMotorThrottle = function manualMotorThrottle(port, vertical, starbord) {
    var maxdiff = 0;
    maxdiff = Math.max(maxdiff, Math.abs(port - lastSentManualThrottle.port));
    maxdiff = Math.max(maxdiff, Math.abs(vertical - lastSentManualThrottle.vertical));
    maxdiff = Math.max(maxdiff, Math.abs(starbord - lastSentManualThrottle.starbord));
    if (vertical < 0)
      vertical = vertical * 2;
    //make up for async props
    if (maxdiff > 0.001) {
      this.cockpit.socket.emit('motor_test', {
        port: -port * this.power,
        starbord: -starbord * this.power,
        vertical: vertical * this.power
      });
      lastSentManualThrottle.port = port;
      lastSentManualThrottle.vertical = vertical;
      lastSentManualThrottle.starbord = starbord;
    }
  };

  leapmotion.prototype.setCameraTilt = function setCameraTilt(value) {
    this.tilt = value;
    if (this.tilt > 1)
      this.tilt = 1;
    if (this.tilt < -1)
      this.tilt = -1;
    this.cockpit.socket.emit('tilt_update', this.tilt);
  };

  //leapmotion.prototype.adjustCameraTilt = function adjustCameraTilt(value) {
  //  this.tilt += value;
  //  this.setCameraTilt(this.tilt);
  //};
  leapmotion.prototype.adjustCameraTilt = function adjustCameraTilt(value) {
    this.tilt = value;
    this.setCameraTilt(this.tilt);
  };

  leapmotion.prototype.setLights = function setLights(value) {
    this.light = value;
    if (this.light > 1)
      this.light = 1;
    if (this.light < 0)
      this.light = 0;
    this.cockpit.socket.emit('brightness_update', this.light);
  };

  //leapmotion.prototype.adjustLights = function adjustLights(value) {
  //  if (this.light === 0 && value < 0) {
  //    //this code rounds the horn so to speak by jumping from zero to max and vise versa
  //    this.light = 0;  //disabled the round the horn feature
  //  } else if (this.light == 1 && value > 0) {
  //    this.light = 1;  //disabled the round the horn feature
  //  } else {
  //    this.light += value;
  //  }
  //  this.setLights(this.light);
  //};
  leapmotion.prototype.adjustLights = function adjustLights(value) {
    this.light = value;
    this.setLights(this.light);
  };

  leapmotion.prototype.toggleLasers = function toggleLasers() {
    this.cockpit.socket.emit('laser_update');
  };

  leapmotion.prototype.toggleLights = function toggleLights() {
    if (this.light > 0) {
      this.setLights(0);
    } else {
      this.setLights(1);
    }
  };

  leapmotion.prototype.toggleholdHeading = function toggleholdHeading() {
    this.cockpit.socket.emit('holdHeading_toggle');
  };
  leapmotion.prototype.toggleholdDepth = function toggleholdDepth() {
    this.cockpit.socket.emit('holdDepth_toggle');
  };
  leapmotion.prototype.powerOnESCs = function powerOnESCs() {
    this.cockpit.socket.emit('escs_poweron');
  };
  leapmotion.prototype.powerOffESCs = function powerOffESCs() {
    this.cockpit.socket.emit('escs_poweroff');
  };


  leapmotion.prototype.setThrottle = function setThrottle(value) {
    this.positions.throttle = value;
    if (value === 0)
      this.positions.throttle = this.ttrim;
  };


  leapmotion.prototype.setLift = function setLift(value) {
    this.positions.lift = value;
    if (value === 0)
      this.positions.lift = this.vtrim;
  };
  leapmotion.prototype.setYaw = function setYaw(value) {
    this.positions.yaw = value;
  };
  leapmotion.prototype.setPitchControl = function setPitchControl(value) {
    this.positions.pitch = value;
  };
  leapmotion.prototype.setRollControl = function setRollControl(value) {
    this.positions.roll = value;
  };

  leapmotion.prototype.incrimentPowerLevel = function incrimentPowerLevel() {
    var currentPowerLevel = this.bindingModel.thrustfactor();
    currentPowerLevel++;
    if (currentPowerLevel > 5)
      currentPowerLevel = 1;
    this.powerLevel(currentPowerLevel);
  };

  leapmotion.prototype.powerLevel = function powerLevel(value) {

    switch (value) {
      case 1:
        this.power = 0.12;
        break;
      case 2:
        this.power = 0.25;
        break;
      case 3:
        this.power = 0.40;
        break;
      case 4:
        this.power = 0.70;
        break;
      case 5:
        this.power = 1;
        break;
    }
    this.bindingModel.thrustfactor(value);
  };

  leapmotion.prototype.powerLevelAlternative = function powerLevelAlternative(value) {
    this.power = value;
    this.bindingModel.thrustfactor(value);
  };



  leapmotion.prototype.sendPilotingData = function sendPilotingData() {
    var positions = this.positions;
    var updateRequired = false;
    //Only send if there is a change
    var controls = {};
    controls.throttle = positions.throttle * this.power;
    controls.yaw = positions.yaw * this.power * 1.5;
    controls.yaw = Math.min(Math.max(controls.yaw, -1), 1);
    controls.lift = positions.lift * this.power;
    controls.pitch = positions.pitch;
    controls.roll = positions.roll;
    for (var i in positions) {
      if (controls[i] != this.priorControls[i]) {
        updateRequired = true;
      }
    }
    if (this.sendUpdateEnabled && updateRequired || this.sendToROVEnabled === false) {
      if (this.sendToROVEnabled) {
        //        this.cockpit.socket.emit('control_update', controls);
        for (var control in controls) {
          if (controls[control] != this.priorControls[control]) {
            this.cockpit.socket.emit(control, controls[control]);
          }
        }
      }
      this.cockpit.emit('leapmotion.control_update', controls);
      this.priorControls = controls;
    }
  };
  leapmotion.prototype.UpdateStatusIndicators = function (status) {
    var rov = this;
    if ('targetDepth' in status) {
      rov.bindingModel.depthHoldEnabled(status.targetDepth != DISABLED);
    }
    if ('targetHeading' in status) {
      rov.bindingModel.headingHoldEnabled(status.targetHeading != DISABLED);
    }
    if ('claser' in status) {
      rov.bindingModel.lasersEnabled(status.claser == 255);
    }
  };
  window.Cockpit.plugins.push(leapmotion);
}(window, jQuery));
